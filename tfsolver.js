var tf = {


  /**
  * Takes a sequence of characters and returns an array of tokens
  */
  tokenizer: function(sequence) {
    console.log('tokenizer');

    var OPERATORS = [
      '−', // negation
      '∙', // conjunction
      '∨', // disjunction
      '⊃', // conditional
      '≡', // biconditional
    ];

    var pos = 0; // starting pos of the variables

    for (var i = 0, len = sequence.length; i < len; i++) {

      // Is this character an operator?
      // FIXME: indexOf is not supported by IE
      if (OPERATORS.indexOf(sequence.charAt(i)) >= 0) {
        console.log(sequence.slice(pos, i).trim());       
        console.log(sequence.charAt(i));
        pos = i + 1;
      }

      // Catch remaining variables
      // FIXME: won't catch closing parenthesis
      if (i == len - 1) {
        console.log(sequence.slice(pos, len).trim());
      }
    }
  }



}
